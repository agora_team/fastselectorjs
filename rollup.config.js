import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

// The main JavaScript bundle for modern browsers that support
// JavaScript modules and other ES2015+ features.
const config = {
  input: 'selector-checkbox.js',
  output: {
    dir: 'build',
    format: 'es',
  },
  plugins: [
    resolve()
  ],
  preserveEntrySignatures: false,
};

if (process.env.NODE_ENV !== 'development') {
  config.plugins.push(terser());
}

export default config;