import {SelectorCheckbox} from '../selector-checkbox.js';
import {fixture, html} from '@open-wc/testing';

const assert = chai.assert;

suite('selector-checkbox', () => {
  test('is defined', () => {
    const el = document.createElement('selector-checkbox');
    assert.instanceOf(el, SelectorCheckbox);
  });

  test('renders with default values', async () => {
    const el = await fixture(html`<selector-checkbox></selector-checkbox>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('renders with a set name', async () => {
    const el = await fixture(html`<selector-checkbox name="Test"></selector-checkbox>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, Test!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('handles a click', async () => {
    const el = (await fixture(html`<selector-checkbox></selector-checkbox>`)) as SelectorCheckbox;
    const button = el.shadowRoot!.querySelector('button')!;
    button.click();
    await el.updateComplete;
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 1</button>
      <slot></slot>
    `
    );
  });
});
