/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
import { LitElement } from 'lit-element';
export declare class SelectorCheckbox extends LitElement {
    list: any[];
    ok: boolean;
    cancel: boolean;
    row_height: string;
    selector_height: string;
    selector_width: string;
    selector_zindex: string;
    search_text: string;
    default_button_text: string;
    hide_button_text: string;
    select_all_text: string;
    items_selected: string;
    selector_display: string;
    all_selected: boolean;
    button_text: string;
    visible_rows: number;
    selector_base_height: number;
    fake_rows_before: number;
    last_scroll_top: number;
    filtered_list: any[];
    firstUpdated(): void;
    private _search;
    private _select_all;
    private _onCheck;
    private _show;
    _onScrollSelector(): Promise<void>;
    private _printVisibleRows;
    private _updateAllSelectedState;
    private _checkAllSelected;
    getSelected(): Number[];
    select(ids: any): void;
    deselect(ids: any): void;
    toggleSelectAll(): void;
    _onChange(): void;
    _onOk(): void;
    _onCancel(): void;
    render(): import("lit-element").TemplateResult;
}
//# sourceMappingURL=selector-checkbox.d.ts.map