---
layout: page.11ty.cjs
title: <selector-checkbox> ⌲ Home
---

# &lt;selector-checkbox>

`<selector-checkbox>` is an awesome element. It's a great introduction to building web components with LitElement, with nice documentation site as well.

## As easy as HTML

<section class="columns">
  <div>

`<selector-checkbox>` is just an HTML element. You can it anywhere you can use HTML!

```html
<selector-checkbox></selector-checkbox>
```

  </div>
  <div>

<selector-checkbox></selector-checkbox>

  </div>
</section>

## Configure with attributes

<section class="columns">
  <div>

`<selector-checkbox>` can be configured with attributed in plain HTML.

```html
<selector-checkbox name="HTML"></selector-checkbox>
```

  </div>
  <div>

<selector-checkbox name="HTML"></selector-checkbox>

  </div>
</section>

## Declarative rendering

<section class="columns">
  <div>

`<selector-checkbox>` can be used with declarative rendering libraries like Angular, React, Vue, and lit-html

```js
import {html, render} from 'lit-html';

const name="lit-html";

render(html`
  <h2>This is a &lt;selector-checkbox&gt;</h2>
  <selector-checkbox .name=${name}></selector-checkbox>
`, document.body);
```

  </div>
  <div>

<h2>This is a &lt;selector-checkbox&gt;</h2>
<selector-checkbox name="lit-html"></selector-checkbox>

  </div>
</section>
