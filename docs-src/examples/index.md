---
layout: example.11ty.cjs
title: <selector-checkbox> ⌲ Examples ⌲ Basic
tags: example
name: Basic
description: A basic example
---

<style>
  selector-checkbox p {
    border: solid 1px blue;
    padding: 8px;
  }
</style>
<selector-checkbox>
  <p>This is child content</p>
</selector-checkbox>

<h3>CSS</h3>

```css
  p {
    border: solid 1px blue;
    padding: 8px;
  }
```

<h3>HTML</h3>

```html
<selector-checkbox>
  <p>This is child content</p>
</selector-checkbox>
```
