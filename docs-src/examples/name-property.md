---
layout: example.11ty.cjs
title: <selector-checkbox> ⌲ Examples ⌲ Name Property
tags: example
name: Name Property
description: Setting the name property
---

<selector-checkbox name="Earth"></selector-checkbox>

<h3>HTML</h3>

```html
<selector-checkbox name="Earth"></selector-checkbox>
```
