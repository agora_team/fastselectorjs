/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property } from 'lit-element';
let SelectorCheckbox = class SelectorCheckbox extends LitElement {
    constructor() {
        super(...arguments);
        this.list = [];
        this.ok = false;
        this.cancel = false;
        this.row_height = '21px';
        this.selector_height = '504px';
        this.selector_width = '300px';
        this.selector_zindex = '999';
        this.search_text = 'Search...';
        this.default_button_text = 'Select course';
        this.hide_button_text = 'Hide';
        this.select_all_text = 'Select all';
        this.items_selected = "items selected";
        this.selector_display = 'none';
        this.all_selected = false;
        this.button_text = '';
        this.visible_rows = 0;
        this.selector_base_height = 0;
        this.fake_rows_before = 0;
        this.last_scroll_top = 0;
        this.filtered_list = [];
    }
    //on init
    firstUpdated() {
        var _a;
        if (this.all_selected) {
            let main_selector;
            main_selector = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelectorAll('div.selector-checkbox-all input')[0];
            main_selector.checked = true;
            for (let element of this.list) {
                element.checked = true;
            }
        }
        this.filtered_list = this.list;
        this.selector_base_height = this.list.length * parseInt(this.row_height.slice(0, -2));
        this.visible_rows = parseInt(this.selector_height.slice(0, -2)) / parseInt(this.row_height.slice(0, -2)) * 3;
        window.addEventListener('click', (e) => {
            const target = e.target;
            if (!this.contains(target) && this.selector_display == 'block') {
                this._show();
            }
        }, false);
    }
    //private functions
    _search(event) {
        var _a;
        var search = event.target.value;
        search = typeof search === 'string' ? search.toUpperCase() : '';
        this.filtered_list = [];
        this.all_selected = true;
        let visible_elements = 0;
        for (let element of this.list) {
            let string = element.name.toUpperCase();
            if (!string.includes(search))
                element.visible = "none";
            else {
                element.visible = "inherit";
                visible_elements++;
                this.filtered_list.push(element);
            }
            if (!element.hasOwnProperty('checked'))
                element.checked = false;
            if (!element.checked && element.visible == "inherit") {
                this.all_selected = false;
            }
        }
        this._updateAllSelectedState();
        this.selector_base_height = visible_elements * parseInt(this.row_height.slice(0, -2));
        let dropdown = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('#scroll-container');
        if (!(dropdown == undefined)) {
            dropdown.scrollTo(0, 0);
        }
        this.requestUpdate();
    }
    _select_all() {
        let counter = 0;
        for (let element of this.list) {
            if (!element.hasOwnProperty('visible'))
                element.visible = 'inherit';
            if (!element.checked && element.visible == 'inherit')
                this.all_selected = false;
            if (element.visible == 'inherit')
                element.checked = true;
            counter++;
        }
        if (this.all_selected && counter != 0) {
            this.all_selected = false;
            for (let element of this.list) {
                if (element.visible == 'inherit')
                    element.checked = false;
            }
        }
        else {
            this.all_selected = true;
        }
        this._updateAllSelectedState();
        this.requestUpdate();
        this._onChange();
    }
    _onCheck(index) {
        this.filtered_list[index].checked = !this.filtered_list[index].checked;
        this._checkAllSelected();
        this._updateAllSelectedState();
        this.requestUpdate();
        this._onChange();
    }
    _show() {
        if (this.selector_display == 'none') {
            this.selector_display = 'block';
            this.button_text = this.hide_button_text;
        }
        else {
            this.selector_display = 'none';
            this.button_text = this.default_button_text;
        }
    }
    async _onScrollSelector() {
        var _a;
        let dropdown = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('#scroll-container');
        if (dropdown == undefined)
            return;
        let difference_between_scrolls = Math.abs(dropdown.scrollTop - this.last_scroll_top);
        if (difference_between_scrolls >= (this.visible_rows / 3 * parseInt(this.row_height.slice(0, -2)))) {
            this.last_scroll_top = dropdown.scrollTop;
            await this.requestUpdate();
        }
    }
    _printVisibleRows() {
        let rows = [];
        let width = parseInt(this.selector_width.slice(0, -2)) - 40 + 'px';
        for (let i = this.fake_rows_before; i < Math.min(this.fake_rows_before + this.visible_rows, this.filtered_list.length); i++) {
            let item = this.filtered_list[i];
            rows.push(html `
      <div
        class="selector-checkbox-listrow"
        style="position:absolute; top:${i * parseInt(this.row_height.slice(0, -2))}px; height:${this.row_height}; display:${item.hasOwnProperty('visible') ? item.visible : 'inherit'};"
      >
        <input
          type="checkbox"
          id="${item.id}"
          value="${item.id}"
          .checked="${item.hasOwnProperty('checked') ? item.checked : false}"
          @click="${() => this._onCheck(i)}"
        />
        <label class="selector-checkbox-label" style="width:${width};white-space:nowrap;display:inline-block;" for="${item.name}">${item.name}</label>
      </div>
    `);
        }
        ;
        return rows;
    }
    _updateAllSelectedState() {
        var _a;
        let main_selector;
        main_selector = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelectorAll('div.selector-checkbox-all input')[0];
        if (main_selector) {
            if (this.all_selected)
                main_selector.checked = true;
            else
                main_selector.checked = false;
        }
    }
    _checkAllSelected() {
        this.all_selected = true;
        for (let element of this.list) {
            if (!element.hasOwnProperty('visible'))
                element.visible = 'inherit';
            if (!element.checked && element.visible == "inherit") {
                this.all_selected = false;
                break;
            }
        }
    }
    //public functions
    getSelected() {
        let result = [];
        for (let element of this.list) {
            if (element.checked)
                result.push(element.id);
        }
        return result;
    }
    select(ids) {
        if (!Array.isArray(ids))
            return;
        for (let i = 0; i < ids.length; i++) {
            if (Number.isInteger(ids[i]))
                ids[i] = ids[i].toString();
        }
        for (let element of this.list) {
            if (ids.includes((element.id).toString())) {
                element.checked = true;
            }
        }
        this._checkAllSelected();
        this._updateAllSelectedState();
        this.requestUpdate();
        this._onChange();
    }
    deselect(ids) {
        if (!Array.isArray(ids))
            return;
        for (let i = 0; i < ids.length; i++) {
            if (Number.isInteger(ids[i]))
                ids[i] = ids[i].toString();
        }
        for (let element of this.list) {
            if (ids.includes((element.id).toString())) {
                element.checked = false;
            }
        }
        this._checkAllSelected();
        this._updateAllSelectedState();
        this.requestUpdate();
        this._onChange();
    }
    toggleSelectAll() {
        this._select_all();
    }
    //events
    _onChange() {
        let event = new CustomEvent('onChange');
        this.dispatchEvent(event);
    }
    _onOk() {
        this._show();
        let event = new CustomEvent('onOk');
        this.dispatchEvent(event);
    }
    _onCancel() {
        this._show();
        let event = new CustomEvent('onCancel');
        this.dispatchEvent(event);
    }
    //render
    render() {
        var _a;
        if (this.button_text == '')
            this.button_text = this.default_button_text;
        let selector_dynamic_height = Math.min(parseInt(this.selector_height.slice(0, -2)), this.filtered_list.length * parseInt(this.row_height.slice(0, -2))) + 'px';
        let dropdown;
        dropdown = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('#scroll-container');
        if (!(dropdown == undefined)) {
            this.fake_rows_before = Math.floor(this.last_scroll_top / parseInt(this.row_height.slice(0, -2))) - this.visible_rows / 3;
            this.fake_rows_before = Math.min(this.filtered_list.length - this.visible_rows, this.fake_rows_before);
            this.fake_rows_before = Math.max(0, this.fake_rows_before);
        }
        return html `
      <div>
        <button
          style="width:${this.selector_width}; margin-bottom: 3px;"
          @click="${() => this._show()}">${this.button_text} (${this.getSelected().length} ${this.items_selected})
        </button>
      </div>
      <div id="drop-down" style="display:${this.selector_display}; position:absolute; z-index:${this.selector_zindex}; background-color:#FFF">
        <div >
          <input
            type="text"
            placeholder="${this.search_text}"
            id="filter"
            @keyup=${this._search}
          />
        </div>
        <div class="selector-checkbox-all">
          <input
            type="checkbox"
            id="-1"
            value="-1"
            @change=${this._select_all}
          />
          <label for="select_all">${this.select_all_text}</label>
        </div>
        <div id="scroll-container" 
          style="height:${selector_dynamic_height}; width:${this
            .selector_width}; overflow:overlay; padding-bottom:10px;"
            @scroll=${this._onScrollSelector}
        >
          <div style="position:relative; height:${this.selector_base_height}px;max-height:${this.selector_base_height}px" >
              ${this._printVisibleRows()}
          </div>
        </div>
        <div>
          <label>${this.getSelected().length} ${this.items_selected}</label>
        </div>
        ${this.ok ? html `<button @click="${this._onOk}">OK</button>` : html ``}
        ${this.cancel
            ? html `<button @click="${this._onCancel}"> Cancel </button>`
            : html ``}
      </div>
    `;
    }
};
__decorate([
    property({ type: Array })
], SelectorCheckbox.prototype, "list", void 0);
__decorate([
    property({ type: Boolean })
], SelectorCheckbox.prototype, "ok", void 0);
__decorate([
    property({ type: Boolean })
], SelectorCheckbox.prototype, "cancel", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "row_height", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "selector_height", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "selector_width", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "selector_zindex", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "search_text", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "default_button_text", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "hide_button_text", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "select_all_text", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "items_selected", void 0);
__decorate([
    property({ type: String })
], SelectorCheckbox.prototype, "selector_display", void 0);
__decorate([
    property({ type: Boolean })
], SelectorCheckbox.prototype, "all_selected", void 0);
SelectorCheckbox = __decorate([
    customElement('selector-checkbox')
], SelectorCheckbox);
export { SelectorCheckbox };
//# sourceMappingURL=selector-checkbox.js.map